package com.eon.prueba.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import com.eon.prueba.dto.agregarEmpleadoDTO;
import com.eon.prueba.dto.empleadosPuestoDTO;
import com.eon.prueba.dto.hrsTrbjDTO;
import com.eon.prueba.dto.pagosDTO;
import com.eon.prueba.service.empleadoServ;
import com.eon.prueba.vo.agregaHrsTrbjVO;
import com.eon.prueba.vo.agregarEmpleadoVO;
import com.eon.prueba.vo.hrsTrbjVO;
import com.eon.prueba.vo.puestoVO;
/**
 * Se definen las rutas de acceso a los servicios de empleado
 * @author sfabilar
 *
 */
@Controller
public class EmpleadosController {
	
	@Autowired
	private empleadoServ empleadoserv;
	
	@PostMapping("/empleados")
	public ResponseEntity<agregarEmpleadoDTO> agregaEmpleado(@Validated @RequestBody agregarEmpleadoVO empleado) {
		return new ResponseEntity<agregarEmpleadoDTO>(empleadoserv.agregaEmpleado(empleado),HttpStatus.OK);
	}
	@PostMapping("/empleados/horas-trabajadas")
	public ResponseEntity<agregarEmpleadoDTO> agregaHrsTrbj(@Validated @RequestBody agregaHrsTrbjVO horas) {
		return new ResponseEntity<agregarEmpleadoDTO>(empleadoserv.agregaHrsTrbj(horas),HttpStatus.OK);
	}
	@GetMapping("/empleados")
	public ResponseEntity<empleadosPuestoDTO> obtieneEmpleadosPuesto(@Validated @RequestBody puestoVO puesto){
		return new ResponseEntity<empleadosPuestoDTO>(empleadoserv.obtieneEmpleadosPuesto(puesto),HttpStatus.OK);
	}
	@GetMapping("/empleados/horas-trabajadas")
	public ResponseEntity<hrsTrbjDTO> totalHorsTrbjEmpl(@Validated @RequestBody hrsTrbjVO hrs){
		return new ResponseEntity<hrsTrbjDTO>(empleadoserv.totalHorsTrbjEmpl(hrs),HttpStatus.OK);
	}
	@GetMapping("/empleados/pagos")
	public ResponseEntity<pagosDTO> totalPagoEmpl(@Validated @RequestBody hrsTrbjVO hrs){
		return new ResponseEntity<pagosDTO>(empleadoserv.totalPagoEmpl(hrs),HttpStatus.OK);
	}
}
