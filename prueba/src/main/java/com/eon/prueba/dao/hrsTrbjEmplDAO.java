package com.eon.prueba.dao;

import java.sql.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import com.eon.prueba.entity.horasTrabajadasEmpleadoEntity;

@Repository
public interface hrsTrbjEmplDAO extends JpaRepository<horasTrabajadasEmpleadoEntity,Integer>{
	/**
	 * Obtiene las horas que un empelado trabajó en una fecha
	 * @param id
	 * @param fechaTrabajada
	 * @return
	 */
	public horasTrabajadasEmpleadoEntity findByEmployeeIdAndFechaTrabajada(int id,Date fechaTrabajada);
	/**
	 * Obtiene la suma de horas trabajadas de un empleado en un rango de fechas
	 * @param id_emp
	 * @param inicio
	 * @param fin
	 * @return
	 */
	@Query(value = "select  sum(worked_hours) as total_quantity from employee_worked_hours " +
            "where employee_id = ?1 and worked_date >= ?2 and worked_date <= ?3", nativeQuery = true)
	public String totalHoras(int id_emp,Date inicio,Date fin);
	/**
	 * Obtiene una lista de horas trabajadas de un empleado entre un rango de fechas
	 * @param des
	 * @param has
	 * @param id
	 * @return
	 */
	public List<horasTrabajadasEmpleadoEntity> findByFechaTrabajadaBetweenAndEmployeeId(Date des,Date has,int id);
	
}