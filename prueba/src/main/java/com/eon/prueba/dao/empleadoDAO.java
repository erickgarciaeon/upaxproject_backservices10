package com.eon.prueba.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.eon.prueba.entity.empleadoEntity;

@Repository
public interface empleadoDAO extends JpaRepository<empleadoEntity,Integer>{
	/**
	 * Encuentra empleado por nombre y apellido
	 * @param nombre
	 * @param apellido
	 * @return
	 */
	public empleadoEntity findByNombreAndApellido(String nombre,String apellido);
	/**
	 * Obtiene lista de empleados que están en un puesto determinado
	 * @param jobId
	 * @return
	 */
	public List<empleadoEntity> findByJobId(int jobId);
}
